# Electron WhatsDesk

Electron WhatsDesk is a **unofficial** client of Whatsapp

This project only insert a web whatsapp in a electron app and add desktop notification

## Download

+ [Desktop .deb](https://villcabo.gitlab.io/electron-whatsdesk/)

![Imgur](https://i.imgur.com/CSqicQy.png)

## Licences

This project uses app icon from [Thoseicons.com](https://thoseicons.com/) under
[Creative Commons Licence v3.0](https://creativecommons.org/licenses/by/3.0/) and modified tray icon from
[EDT.im](http://edt.im) under [Creative Commons Licence v2.5](https://creativecommons.org/licenses/by/2.5/).
